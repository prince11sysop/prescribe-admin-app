package com.wellnexus.technologies.prescribeadmin.Models;

public class Appointment {
    private boolean adminBooked;
    private String age;
    private String date;
    private String doctor;
    private String doctorId;
    private String flag;
    private String highlights;
    private String hosp_id;
    private String name;
    private String phone;
    private String pid;
    private int queue;
    private int slot;

    public Appointment(){}
    public Appointment(boolean adminBooked, String age, String date, String doctor, String doctorId, String flag, String highlights, String hosp_id, String name, String phone, String pid, int queue, int slot) {
        this.adminBooked = adminBooked;
        this.age = age;
        this.date = date;
        this.doctor = doctor;
        this.doctorId = doctorId;
        this.flag = flag;
        this.highlights = highlights;
        this.hosp_id = hosp_id;
        this.name = name;
        this.phone = phone;
        this.pid = pid;
        this.queue = queue;
        this.slot = slot;
    }

    public boolean isAdminBooked() {
        return adminBooked;
    }

    public void setAdminBooked(boolean adminBooked) {
        this.adminBooked = adminBooked;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getFlag() {
        return flag;
    }

    public void setFlag(String flag) {
        this.flag = flag;
    }

    public String getHighlights() {
        return highlights;
    }

    public void setHighlights(String highlights) {
        this.highlights = highlights;
    }

    public String getHosp_id() {
        return hosp_id;
    }

    public void setHosp_id(String hosp_id) {
        this.hosp_id = hosp_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPid() {
        return pid;
    }

    public void setPid(String pid) {
        this.pid = pid;
    }

    public int getQueue() {
        return queue;
    }

    public void setQueue(int queue) {
        this.queue = queue;
    }

    public int getSlot() {
        return slot;
    }

    public void setSlot(int slot) {
        this.slot = slot;
    }
}
