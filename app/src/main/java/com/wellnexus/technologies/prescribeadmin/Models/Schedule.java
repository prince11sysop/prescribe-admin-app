package com.wellnexus.technologies.prescribeadmin.Models;

public class Schedule {
    private String designation;
    private String doctor;
    private String doctorId;
    private String slot;

    public Schedule(){}

    public Schedule(String designation, String doctor, String doctorId, String slot) {
        this.designation = designation;
        this.doctor = doctor;
        this.doctorId = doctorId;
        this.slot = slot;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getSlot() {
        return slot;
    }

    public void setSlot(String slot) {
        this.slot = slot;
    }
}
