package com.wellnexus.technologies.prescribeadmin.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.wellnexus.technologies.prescribeadmin.Activity.HospitalsListActivity;
import com.wellnexus.technologies.prescribeadmin.R;

public class HospitalFragment extends Fragment {

    private TextView hospitalCount;

    public View onCreateView(@NonNull LayoutInflater inflater,ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_hospitals, container, false);

        hospitalCount=root.findViewById(R.id.hospitalCount);

        hospitalCount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), HospitalsListActivity.class));
            }
        });

        return root;
    }
}