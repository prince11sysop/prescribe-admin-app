package com.wellnexus.technologies.prescribeadmin.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wellnexus.technologies.prescribeadmin.Adapters.DoctorsListAdapter;
import com.wellnexus.technologies.prescribeadmin.Adapters.HospitalsListAdapter;
import com.wellnexus.technologies.prescribeadmin.Adapters.ScheduleAdapter;
import com.wellnexus.technologies.prescribeadmin.Fragments.DoctorsFragment;
import com.wellnexus.technologies.prescribeadmin.Models.Appointment;
import com.wellnexus.technologies.prescribeadmin.Models.Schedule;
import com.wellnexus.technologies.prescribeadmin.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TreeMap;

public class DoctorsListActivity extends AppCompatActivity {

    DatabaseReference mMessagesDatabaseReference;
    RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctors_list);
        setTitle("Doctors");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        recyclerView=findViewById(R.id.recycler_view);
        progressBar=findViewById(R.id.progressBar);
        progressBar.setVisibility(View.GONE);

        String count=getIntent().getStringExtra("COUNT");
        int size=Integer.parseInt(count);

        ArrayList<Schedule> localList=new ArrayList<>();
        for(int i=0;i<size;i++){
            localList.add(DoctorsFragment.appointmentsList.get(i));
        }

//        mMessagesDatabaseReference = FirebaseDatabase.getInstance().getReference().child("schedule");
//        final List<Schedule> appointmentsList = new ArrayList<>();
        layoutManager = new GridLayoutManager(this, 1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);

//        DividerItemDecoration dividerItemDecoration=new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
//        recyclerView.addItemDecoration(dividerItemDecoration);
//
//        final TreeMap<String, ArrayList<Schedule>> scheduleList=new TreeMap<>();

//        mMessagesDatabaseReference.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot snapshot) {
//
//                for(DataSnapshot dataSnapshot:snapshot.getChildren()){
//
//                    final String date=dataSnapshot.getKey();
//                    ArrayList<Schedule> todaySchedule=new ArrayList<>();
//
//                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
//
//
//                        Schedule schedule = dataSnapshot1.getValue(Schedule.class);
//                        todaySchedule.add(schedule);
////                        appointmentsList.add(projectDetails);
//                    }
//                    scheduleList.put(date,todaySchedule);
//                }


                Toast.makeText(DoctorsListActivity.this, "Total "+count+" Doctors", Toast.LENGTH_SHORT).show();
                ScheduleAdapter adapter = new ScheduleAdapter(getApplicationContext(), localList);
                recyclerView.setAdapter(adapter);
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError databaseError) {
//
//            }
//        });



    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
