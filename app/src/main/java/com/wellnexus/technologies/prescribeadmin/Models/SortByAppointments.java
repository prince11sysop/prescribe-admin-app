package com.wellnexus.technologies.prescribeadmin.Models;

import java.util.Comparator;

public class SortByAppointments implements Comparator<Appointment> {
    @Override
    public int compare(Appointment appointment, Appointment t1) {
        int age1=Integer.parseInt(appointment.getAge());
        int age2=Integer.parseInt(t1.getAge());

//        return appointment.getAge().compareTo(t1.getAge());
        return age2-age1;
    }
}
