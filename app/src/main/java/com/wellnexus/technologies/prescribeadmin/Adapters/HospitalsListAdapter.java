package com.wellnexus.technologies.prescribeadmin.Adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.wellnexus.technologies.prescribeadmin.Activity.HospitalDetailsActivity;
import com.wellnexus.technologies.prescribeadmin.Models.Appointment;
import com.wellnexus.technologies.prescribeadmin.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class HospitalsListAdapter extends RecyclerView.Adapter<HospitalsListAdapter.HospitalViewHolder> {

    private Context myContext;
    private List<Appointment> hospitalsList;

    public HospitalsListAdapter(Context myContext, List<Appointment> hospitalsList){
        this.myContext = myContext;
        this.hospitalsList = hospitalsList;
    }

    @NonNull
    @Override
    public HospitalsListAdapter.HospitalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater = LayoutInflater.from(myContext);
        View view = layoutInflater.inflate(R.layout.card_format_hospitals_list, null);

        return new HospitalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HospitalViewHolder hospitalViewHolder, final int position) {

        final Appointment appointment = hospitalsList.get(position);

        hospitalViewHolder.hospName.setText(appointment.getName());
        hospitalViewHolder.hospId.setText(appointment.getHosp_id());
        hospitalViewHolder.appointments.setText(appointment.getAge());
//        hospitalViewHolder.appointments.setText(appointment.getSlot());

        hospitalViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, HospitalDetailsActivity.class);
                intent.putExtra("NAME",appointment.getName() );
                intent.putExtra("ID",appointment.getHosp_id() );
                intent.putExtra("COUNT",appointment.getAge() );
                myContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return hospitalsList.size();
    }


    public static class HospitalViewHolder extends RecyclerView.ViewHolder {

        TextView hospName,hospId, appointments, docOnborded;
        CardView cardView;


        public HospitalViewHolder(@NonNull final View itemView) {
            super(itemView);
            hospName = itemView.findViewById(R.id.hospName);
            hospId = itemView.findViewById(R.id.hospId);
            appointments = itemView.findViewById(R.id.appointments);
            docOnborded = itemView.findViewById(R.id.docOnborded);
            cardView=itemView.findViewById(R.id.cardView);
        }

    }

}

