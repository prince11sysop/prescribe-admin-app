package com.wellnexus.technologies.prescribeadmin.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wellnexus.technologies.prescribeadmin.Adapters.HospitalsListAdapter;
import com.wellnexus.technologies.prescribeadmin.Models.Appointment;
import com.wellnexus.technologies.prescribeadmin.Models.SortByAppointments;
import com.wellnexus.technologies.prescribeadmin.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HospitalsListActivity extends AppCompatActivity {

    DatabaseReference mMessagesDatabaseReference;
    RecyclerView recyclerView;
    GridLayoutManager layoutManager;
    ProgressBar progressBar;
    List<Appointment> appointmentsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospitals_list);
        setTitle("Hospitals List");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        recyclerView=findViewById(R.id.recycler_view);
        progressBar=findViewById(R.id.progressBar);


        mMessagesDatabaseReference = FirebaseDatabase.getInstance().getReference().child("appointment");
        appointmentsList = new ArrayList<>();
        layoutManager = new GridLayoutManager(this, 1);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setHasFixedSize(true);



        DividerItemDecoration dividerItemDecoration=new DividerItemDecoration(this,DividerItemDecoration.VERTICAL);
        recyclerView.addItemDecoration(dividerItemDecoration);

        mMessagesDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for(DataSnapshot dataSnapshot:snapshot.getChildren()){
                    //loop 1 to go through all child nodes of appointments
                    final String date=dataSnapshot.getKey();

                    if(date.equals("01-03-2020") || date.equals("01-02-2020")|| date.equals("01-04-2020")|| date.equals("01-10-2019")||
                            date.equals("01-11-2019") || date.equals("01-12-2019")|| date.equals("02-01-2020")|| date.equals("02-02-2020")||
                            date.equals("02-11-2019") || date.equals("03-10-2019")|| date.equals("03-11-2019")|| date.equals("03-12-2019")){
                        for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                            //loop 2 to get the doctors id of each day

                            for(DataSnapshot dataSnapshot2: dataSnapshot1.getChildren()){
                                Appointment projectDetails = dataSnapshot2.getValue(Appointment.class);
                                appointmentsList.add(projectDetails);
                            }
                        }
                    }
                }
                progressBar.setVisibility(View.GONE);
                HospitalsListAdapter adapter = new HospitalsListAdapter(getApplicationContext(), appointmentsList);
                recyclerView.setAdapter(adapter);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.docOnborded:

                return true;
            case R.id.appointments:
                Collections.sort(appointmentsList,new SortByAppointments());
                HospitalsListAdapter adapter = new HospitalsListAdapter(getApplicationContext(), appointmentsList);
                recyclerView.setAdapter(adapter);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
