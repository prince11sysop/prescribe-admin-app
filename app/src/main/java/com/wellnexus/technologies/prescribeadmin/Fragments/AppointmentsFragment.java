package com.wellnexus.technologies.prescribeadmin.Fragments;

import android.graphics.Color;
import android.graphics.DashPathEffect;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.util.EventLogTags;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.interfaces.datasets.IPieDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.Utils;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.database.core.utilities.Tree;
import com.wellnexus.technologies.prescribeadmin.Models.Appointment;
import com.wellnexus.technologies.prescribeadmin.R;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;

public class AppointmentsFragment extends Fragment {

    DatabaseReference mMessagesDatabaseReference;
    ProgressBar progressBar;
    TextView todayCount, weeklyCount, monthlyCount;
    BarChart chart;
    LineChart lineChart;
    CardView daily,weekly,monthly;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_appointments, container, false);
        todayCount=root.findViewById(R.id.todayCount);
        weeklyCount=root.findViewById(R.id.weeklyCount);
        monthlyCount=root.findViewById(R.id.monthlyCount);
        chart= root.findViewById(R.id.barChart);
        lineChart= root.findViewById(R.id.lineChart);
        daily=root.findViewById(R.id.daily);
        weekly=root.findViewById(R.id.weekly);
        monthly=root.findViewById(R.id.monthly);
        progressBar=root.findViewById(R.id.progressBar);

        mMessagesDatabaseReference = FirebaseDatabase.getInstance().getReference().child("appointment");
        final List<Appointment> appointmentsList = new ArrayList<>();
        final TreeMap<String,Integer> appointmentCount=new TreeMap<>();

        mMessagesDatabaseReference.addValueEventListener(new ValueEventListener() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for(DataSnapshot dataSnapshot:snapshot.getChildren()){
                    //loop 1 to go through all child nodes of appointments
                    final String date=dataSnapshot.getKey();
                    if(date.equals("01-03-2020") || date.equals("01-02-2020")|| date.equals("01-04-2020")|| date.equals("01-10-2019")||
                            date.equals("01-11-2019") || date.equals("01-12-2019")|| date.equals("02-01-2020")|| date.equals("02-02-2020")||
                            date.equals("02-11-2019") || date.equals("03-10-2019")|| date.equals("03-11-2019")|| date.equals("03-12-2019")){
                        for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                            //loop 2 to get the doctors id of each day

                            for(DataSnapshot dataSnapshot2: dataSnapshot1.getChildren()){
                                Appointment projectDetails = dataSnapshot2.getValue(Appointment.class);
                                appointmentsList.add(projectDetails);
                            }
                        }
                    }
                }
//                Toast.makeText(getActivity(), ""+appointmentsList.size(), Toast.LENGTH_SHORT).show();
                for(int i=0;i<appointmentsList.size();i++){
                    String date=appointmentsList.get(i).getDate();
                    if(appointmentCount.containsKey(date)){
                        int val=appointmentCount.get(date);
                        appointmentCount.put(date,val+1);
                    }
                    else
                        appointmentCount.put(date,  1);
                }


                String currentDate = new SimpleDateFormat("dd-MM-yyyy", Locale.getDefault()).format(new Date());

                if(appointmentCount.containsKey(currentDate))
                    todayCount.setText(appointmentCount.get(currentDate)+"");
                else
                    todayCount.setText("0");

                int weeklyAppointments=createBarChart(appointmentCount);
                weeklyCount.setText(weeklyAppointments+"");

                int monthlyAppointments= createLineChart(appointmentCount);
                monthlyCount.setText(monthlyAppointments+"");
                progressBar.setVisibility(View.GONE);
                daily.setVisibility(View.VISIBLE);
                weekly.setVisibility(View.VISIBLE);
                monthly.setVisibility(View.VISIBLE);

              }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });


        return root;
    }

    public int createBarChart(TreeMap<String,Integer> appointments){

        int totalAppointments=0;
        ArrayList<BarEntry> weeklyAppointment = new ArrayList();
        ArrayList<String> weekDays = new ArrayList();

        int count=0;
        for(Map.Entry entry:appointments.entrySet()){
            String date=(String)entry.getKey();
            int val=(int)entry.getValue();
            totalAppointments+=val;
            weeklyAppointment.add(new BarEntry(val,count));
            weekDays.add(date);
            count++;

            if(count==7) break;
        }

        BarDataSet bardataset = new BarDataSet(weeklyAppointment, "Appointments");
        chart.animateY(1500);
        BarData data = new BarData(weekDays, bardataset);
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        chart.setData(data);

        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setEnabled(false);
        chart.setDescription("");
        chart.getLegend().setEnabled(false);

        return totalAppointments;
    }


    public int createLineChart(TreeMap<String,Integer> appointmentsCount){
        int totalAppointments=0;
        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> months = new ArrayList();
        int count=0;

        for(Map.Entry entry:appointmentsCount.entrySet()){
            String date=(String)entry.getKey();
            int val=(int)entry.getValue();
            totalAppointments+=val;
            entries.add(new BarEntry(val,count));
            months.add(date);
            count++;
        }



        LineDataSet lineDataSet = new LineDataSet(entries, "Appointments");
        lineDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.blue));
        lineDataSet.setValueTextColor(ContextCompat.getColor(getActivity(), R.color.grey));
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);


        LineData lineData = new LineData(months,lineDataSet);
        lineChart.setData(lineData);
        lineChart.invalidate();
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.setDescription("");
        lineChart.getLegend().setEnabled(false);

        return totalAppointments;
    }

}