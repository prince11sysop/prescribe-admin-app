package com.wellnexus.technologies.prescribeadmin.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.wellnexus.technologies.prescribeadmin.Activity.DoctorsListActivity;
import com.wellnexus.technologies.prescribeadmin.Adapters.ScheduleAdapter;
import com.wellnexus.technologies.prescribeadmin.Models.Schedule;
import com.wellnexus.technologies.prescribeadmin.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class DoctorsFragment extends Fragment {

    Button monthlyDoctorsList, weeklyDoctorsList, dailyDoctorsList;
    DatabaseReference mMessagesDatabaseReference;
    ProgressBar progressBar;
    CardView daily,weekly,monthly;
    TextView todayCount, weeklyCount, monthlyCount;
    int count=0;
    int dailyCount=0,last7Days=0,last30Days=0;
    BarChart chart;
    LineChart lineChart;
    public static List<Schedule> appointmentsList;

    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_doctors, container, false);
        monthlyDoctorsList=root.findViewById(R.id.monthlyListBtn);
        weeklyDoctorsList=root.findViewById(R.id.weeklyListBtn);
        dailyDoctorsList=root.findViewById(R.id.dailyListBtn);
        daily=root.findViewById(R.id.daily);
        weekly=root.findViewById(R.id.weekly);
        monthly=root.findViewById(R.id.monthly);
        progressBar=root.findViewById(R.id.progressBar);
        todayCount=root.findViewById(R.id.todayCount);
        weeklyCount=root.findViewById(R.id.weeklyCount);
        monthlyCount=root.findViewById(R.id.monthlyCount);
        chart= root.findViewById(R.id.barChart);
        lineChart= root.findViewById(R.id.lineChart);

        mMessagesDatabaseReference = FirebaseDatabase.getInstance().getReference().child("schedule");
        final TreeMap<String, ArrayList<Schedule>> scheduleList=new TreeMap<>();
        appointmentsList=new ArrayList<>();

        mMessagesDatabaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {

                for(DataSnapshot dataSnapshot:snapshot.getChildren()){

                    final String date=dataSnapshot.getKey();
                    ArrayList<Schedule> todaySchedule=new ArrayList<>();

                    for(DataSnapshot dataSnapshot1:dataSnapshot.getChildren()){
                        Schedule schedule = dataSnapshot1.getValue(Schedule.class);
                        todaySchedule.add(schedule);
                    }
                    scheduleList.put(date,todaySchedule);
                }

                progressBar.setVisibility(View.GONE);
                daily.setVisibility(View.VISIBLE);
                weekly.setVisibility(View.VISIBLE);
                monthly.setVisibility(View.VISIBLE);
//                Toast.makeText(getActivity(), ""+scheduleList.size(), Toast.LENGTH_SHORT).show();

                int size=0;
                for(Map.Entry entry:scheduleList.entrySet()){
                    ArrayList<Schedule> todayAppointment= (ArrayList<Schedule>) entry.getValue();
                    size+=todayAppointment.size();
                    for(int i=0;i<todayAppointment.size();i++)
                        appointmentsList.add(todayAppointment.get(i));

                    if(count==0){
                        todayCount.setText(size+"");
                        dailyCount=size;
                    }
                    if(count==6){
                        weeklyCount.setText(size+"");
                        last7Days=size;
                    }
                    if(count==29){
                        monthlyCount.setText(size+"");
                        last30Days=size;
                    }

                    count++;
                    if(count>=30)
                        break;
                }
                createBarChart(scheduleList);
                createLineChart(scheduleList);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });





        dailyDoctorsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DoctorsListActivity.class);
                intent.putExtra("COUNT", Integer.toString(dailyCount));
                startActivity(intent);
            }
        });

        weeklyDoctorsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DoctorsListActivity.class);
                intent.putExtra("COUNT", Integer.toString(last7Days));
                startActivity(intent);
            }
        });

        monthlyDoctorsList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), DoctorsListActivity.class);
                intent.putExtra("COUNT", Integer.toString(last30Days));
                startActivity(intent);
            }
        });

        return root;
    }

    public void createBarChart(TreeMap<String,ArrayList<Schedule>> appointments){

        ArrayList<BarEntry> weeklyAppointment = new ArrayList();
        ArrayList<String> weekDays = new ArrayList();

        int count=0;
        for(Map.Entry entry:appointments.entrySet()){
            String date=(String)entry.getKey();

            ArrayList<Schedule> tempList= (ArrayList<Schedule>) entry.getValue();
            weeklyAppointment.add(new BarEntry(tempList.size(),count));
            weekDays.add(date);
            count++;

            if(count==7) break;
        }

        BarDataSet bardataset = new BarDataSet(weeklyAppointment, "");
        chart.animateY(1000);
        BarData data = new BarData(weekDays, bardataset);
        bardataset.setColors(ColorTemplate.COLORFUL_COLORS);
        chart.setData(data);

        chart.getAxisLeft().setDrawGridLines(false);
        chart.getXAxis().setDrawGridLines(false);
        chart.getAxisRight().setDrawAxisLine(false);
        chart.getAxisRight().setDrawGridLines(false);
        chart.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        YAxis yAxisRight = chart.getAxisRight();
        yAxisRight.setEnabled(false);
        chart.setDescription("");
        chart.getLegend().setEnabled(false);

    }


    public void createLineChart(TreeMap<String,ArrayList<Schedule>>  appointmentsCount){

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> months = new ArrayList();
        int count=0;

        for(Map.Entry entry:appointmentsCount.entrySet()){
            String date=(String)entry.getKey();
            ArrayList<Schedule> tempList= (ArrayList<Schedule>) entry.getValue();

            entries.add(new BarEntry(tempList.size(),count));
            months.add(date);
            count++;

            if(count==30) break;
        }


        LineDataSet lineDataSet = new LineDataSet(entries, "");
        lineDataSet.setColor(ContextCompat.getColor(getActivity(), R.color.blue));
        lineDataSet.setValueTextColor(ContextCompat.getColor(getActivity(), R.color.grey));
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        LineData lineData = new LineData(months,lineDataSet);
        lineChart.setData(lineData);
        lineChart.invalidate();
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.setDescription("");
        lineChart.getLegend().setEnabled(false);
    }


}