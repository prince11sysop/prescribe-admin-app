package com.wellnexus.technologies.prescribeadmin.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.wellnexus.technologies.prescribeadmin.Activity.DoctorDetailActivity;
import com.wellnexus.technologies.prescribeadmin.Models.Schedule;
import com.wellnexus.technologies.prescribeadmin.R;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.HospitalViewHolder> {
    private Context myContext;
    private List<Schedule> hospitalsList;

    public ScheduleAdapter(Context myContext, List<Schedule> hospitalsList){
        this.myContext = myContext;
        this.hospitalsList = hospitalsList;
    }

    @NonNull
    @Override
    public ScheduleAdapter.HospitalViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        LayoutInflater layoutInflater = LayoutInflater.from(myContext);
        View view = layoutInflater.inflate(R.layout.card_doctors_list, null);

        return new HospitalViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final HospitalViewHolder hospitalViewHolder, final int position) {

        final Schedule schedule = hospitalsList.get(position);

        hospitalViewHolder.hospName.setText(schedule.getDoctor()); //doctor id
        hospitalViewHolder.hospId.setText(schedule.getDesignation());
//        hospitalViewHolder.appointments.setText(appointment.getAge());
//        hospitalViewHolder.appointments.setText(appointment.getSlot());

        hospitalViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(myContext, DoctorDetailActivity.class);
                intent.putExtra("NAME",schedule.getDoctor() );
                intent.putExtra("ID", schedule.getDoctorId());
                intent.putExtra("DESIGNATION",schedule.getDesignation() );
                intent.putExtra("SLOT", schedule.getSlot());
                myContext.startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return hospitalsList.size();
    }


    public static class HospitalViewHolder extends RecyclerView.ViewHolder {

        TextView hospName,hospId, appointments, docOnborded;
        CardView cardView;


        public HospitalViewHolder(@NonNull final View itemView) {
            super(itemView);
            hospName = itemView.findViewById(R.id.hospName);
            hospId = itemView.findViewById(R.id.hospId);
            appointments = itemView.findViewById(R.id.appointments);
            docOnborded = itemView.findViewById(R.id.docOnborded);
            cardView=itemView.findViewById(R.id.cardView);
        }

    }

}
