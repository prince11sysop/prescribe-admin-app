package com.wellnexus.technologies.prescribeadmin.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.os.Bundle;
import android.widget.TextView;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.wellnexus.technologies.prescribeadmin.Models.Schedule;
import com.wellnexus.technologies.prescribeadmin.R;

import java.util.ArrayList;
import java.util.Map;
import java.util.TreeMap;

public class HospitalDetailsActivity extends AppCompatActivity {

    LineChart lineChart;
    TextView hospName,hospId,totalDoctors;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_details);
        lineChart= findViewById(R.id.lineChart);
        hospName=findViewById(R.id.hospName);
        hospId=findViewById(R.id.hospId);
        totalDoctors=findViewById(R.id.totalDoctors);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle("Hospital Detail");

        createLineChart();

        hospName.setText(getIntent().getStringExtra("NAME"));
        hospId.setText("Hospital Id: "+getIntent().getStringExtra("ID"));
        totalDoctors.setText("Total Doctors: "+getIntent().getStringExtra("COUNT"));

    }

    public void createLineChart(){

        ArrayList<Entry> entries = new ArrayList<>();
        ArrayList<String> months = new ArrayList();
        int count=0;

//        for(Map.Entry entry:appointmentsCount.entrySet()){
//            String date=(String)entry.getKey();
//            ArrayList<Schedule> tempList= (ArrayList<Schedule>) entry.getValue();
//
//            entries.add(new BarEntry(tempList.size(),count));
//            months.add(date);
//            count++;
//
//            if(count==30) break;
//        }

        entries.add(new BarEntry(8,0));
        entries.add(new BarEntry(6,1));
        entries.add(new BarEntry(12,2));
        entries.add(new BarEntry(3,3));
        entries.add(new BarEntry(9,4));
        entries.add(new BarEntry(7,5));
        entries.add(new BarEntry(8,6));


        months.add("Mon");
        months.add("Tue");
        months.add("Wed");
        months.add("Thur");
        months.add("Fri");
        months.add("Sat");
        months.add("Sun");

        LineDataSet lineDataSet = new LineDataSet(entries, "");
        lineDataSet.setColor(ContextCompat.getColor(getApplicationContext(), R.color.blue));
        lineDataSet.setValueTextColor(ContextCompat.getColor(getApplicationContext(), R.color.grey));
        XAxis xAxis = lineChart.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);

        LineData lineData = new LineData(months,lineDataSet);
        lineChart.setData(lineData);
        lineChart.invalidate();
        lineChart.getAxisLeft().setDrawGridLines(false);
        lineChart.getXAxis().setDrawGridLines(false);
        lineChart.setDescription("");
        lineChart.getLegend().setEnabled(false);
    }

    @Override
    public boolean onSupportNavigateUp(){
        finish();
        return true;
    }
}
